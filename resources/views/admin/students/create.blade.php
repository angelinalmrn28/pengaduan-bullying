@extends('app')

@section('content')
    <div class="container">
        <h1>Add new student</h1>
        <form action="/admin/students" method="POST">
            @csrf
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="nis" class="form-label">Nis</label>
                    <input type="text" class="form-control" id="nis" name="nis">
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Class</label>
                <select name="class" class="form-select">
                    @foreach (['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Gender</label>
                <select name="gender" class="form-select">
                    @foreach (['male', 'female'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="name" class="form-label">name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="pasword" name="password">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Level</label>
                <select name="level" class="form-select">
                    @foreach (['admin', 'staff', 'student'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>


            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
             <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection