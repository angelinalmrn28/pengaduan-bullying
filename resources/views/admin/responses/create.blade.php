@extends('app')

@section('content')
    <div class="container">
        <h1>Add new response</h1>
        <form action="/admin/responses" method="POST">
            @csrf
            <div class="col-3 mb-3">
                <label class="form-label">complaint ID</label>
                <select name="complaint_id" class="form-select">
                    @foreach ($complaint_list as $complaint)
                        <option value="{{ $complaint->id }}">{{ $complaint->id}} - {{ $complaint->report_content}}</option>
                    @endforeach
                </select>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="response_date" class="form-label">response_date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="response" class="form-label">response</label>
                    <input type="text" class="form-control" id="response" name="response">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
             <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>

@endsection