<?php
$level = auth()->user()->level;
?>
<header class="p-3 mb-3 border-bottom " id="navigation-bar">
    <div class="container d-flex align-items-center">
        <a href="#" class="d-flex align-items-center m-3 text-decoration-none">
            <img src="/img/photo.png" style="width: 50px">
            <span class="f-4 ms-2">Report Bullying</span>
            <ul class="nav" style="margin-left: -30px">
                <li><a href="/home" class="nav-link" style="margin-top: -35px">Home</a></li>
                <li><a href="/kontak" class="nav-link">Hubungi Kami</a></li>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="me-3 nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Informasi
                        </a>
                        <ul class="dropdown-menu dropdown-menu-dark">
                            <li><a class="dropdown-item" href="/pengertian">Apa itu Bullying</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="/jenis">Jenis Bullying</a></li>
                            <li>
                        </ul>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Pengaduan
                        </a>
                        <ul class="dropdown-menu dropdown-menu-dark">
                            <li><a class="dropdown-item" href="/student/complaints/create">Pengaduan Onlline</a></li>
                            <li>
                            </li>
                        </ul>
                    </li>
                </ul>
                    @if ($level != 'student')
                <li><a href="/{{ $level }}/users" class="nav-link">User</a></li>
                <li><a href="/{{ $level }}/students" class="nav-link">Student</a></li>
                @endif
                <li><a href="/{{ $level }}/complaints" class="nav-link">Complaint</a></li>
                <li><a href="/{{ $level }}/responses" class="nav-link">Response</a></li>
                </ul>
                <div class="dropdown me-auto">
                    <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                        <i class="bi bi-person-circle" style="font-size: 30px"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="dropdown-item">Halo {{ auth()->user()->username }}</a></li>
                        <hr class="dropdown-divider">
                        </li>
                        <li><a href="/logout" class="dropdown-item">Log out</a></li>
                    </ul>
                </div>
    </div>
</header>
