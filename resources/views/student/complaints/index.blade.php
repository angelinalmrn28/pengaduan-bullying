@extends('app')

@section('content')
    <div class="container">
        <h1>Data Pengaduan</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Victim_name</th>
                    <th>Incident_date</th>
                    <th>Location</th>
                    <th>Type_bullying</th>
                    <th>Addressed_to</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $complaint->victim_name}}</td>
                        <td>{{ $complaint->incident_date }}</td>
                        <td>{{ $complaint->location }}</td>
                        <td>{{ $complaint->type_bullying }}</td>
                        <td>{{ $complaint->addressed_to }}</td>
                        <td>{{ $complaint->status }}</td>
                        <td>
                        </td>
                        <td>
                            <a href="/student/complaints/{{ $complaint->id }}" class="btn btn-info">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/student/complaints/create" class="btn btn-success">Add New</a>
    </div>
@endsection