@extends('app')

@section('content')
    <div class="container">
        <h1>Add new complaint</h1>
        <form action="/student/complaints" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                <div class="col-12 mb-12">
                    <label for="victim_name" class="form-label">Victim_name</label>
                    <input type="text" class="form-control" id="victim_name" name="victim_name">
                </div>


                <div class="col-12 mb-12">
                    <label for="incident_date" class="form-label">Incident_date</label>
                    <input type="date" class="form-control" id="incident_date" name="incident_date">
                </div>



                <div class="col-12 mb-12">
                    <label for="location" class="form-label">Location</label>
                    <input type="text" class="form-control" id="location" name="location">
                </div>


                <div class="col-12 mb-12">
                    <label class="form-label">Type_bullying</label>
                    <select name="type_bullying" class="form-select">
                        @foreach (['verbal bullying', 'physical bullying', 'cyber bullying', 'social bullying', 'sexual bullying'] as $item)
                            <option value="{{ $item }}">
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-12 mb-12">
                    <label for="report_content" class="form-label">Report_content</label>
                    <input type="text" class="form-control" id="report_content" name="report_content">
                </div>


                <div class="col-12 mb-12">
                    <label for="image" class="form-label">Image</label>
                    <input type="file" class="form-control" id="image" name="image" accept="image/png,image/jpeg">
                </div>


                <div class="col-12 mb-12">
                    <label for="addresed_to" class="form-label">Addressed_to</label>
                    <input type="text" class="form-control" id="addressed_to" name="addressed_to">
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>

@endsection