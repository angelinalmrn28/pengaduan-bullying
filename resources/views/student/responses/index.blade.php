@extends('app')

@section('content')
    <div class="container">
        <h1>Data Tanggapan</h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>Complaint_id</th>
                    <th>Response_date</th>
                    <th>Response</th>
                    <th>Staff_id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                        <td>{{ $response->staff_id }}</td>
                        <td>
                            <a href="/student/responses/{{ $response->id }}" class="btn btn-info">Detail</a>
                            {{-- <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $response->id }}">Delete</a> --}}
                        </td>
                    </tr>
                    {{-- <div class="modal fade" id="modal-{{ $response->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div> 
                                <div class="modal-body">
                                    <p>Response dengan ID {{ $response->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/student/responses/{{ $response->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                @endforeach
            </tbody>
        </table>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error}}</p>
            @endforeach
        @endif
    </div>
@endsection




{{-- @extends('app')

@section('content')
    <div class="container">
        <h1>Add new response</h1>
        <form action="/student/responses" method="POST">
            @csrf
            <div class="col-3 mb-3">
                <label class="form-label">complaint ID</label>
                <select name="complaint_id" class="form-select">
                    @foreach ($response_list as $complaint)
                        <option value="{{ $response_list }}">{{ $complaint->id}} - {{ $complaint->report_content}}</option>
                    @endforeach
                </select>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="response_date" class="form-label">response_date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="response" class="form-label">response</label>
                    <input type="text" class="form-control" id="response" name="response">
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">staff ID</label>
                <select name="staff_id" class="form-select">
                    @foreach ($response_list as $responses)
                        <option value="{{ $response_list }}">{{ $user->name}} - {{ $user->level}}</option>
                    @endforeach
                </select>
            </div>


            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
             <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>

@endsection --}}