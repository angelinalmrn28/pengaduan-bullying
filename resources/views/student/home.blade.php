@extends('app')

@section('content')
    <h1> </h1>

    <div class="clearfix">
        <img src="/img/foto.jpeg" class="col-md-3 float-md-end mb-3 ms-md-3" alt="...">
      
           <h2> Stop Bullying: Understanding, Preventing, and Taking Action</h2>
           <p> Bullying is a pervasive issue that affects individuals of all ages and backgrounds. 
            It is a problem that can lead to emotional distress, physical harm, and even death in some cases.
             While bullying has been a problem for many years, the rise of technology 
             and social media has made it easier 
             for bullies to target their victims, making it more difficult for victims
              to escape the abuse.
        </p>
         <h3>  What can we do to stop bullying? </h3>
        <p>
            Stopping bullying requires a multifaceted approach that involves education, prevention, and intervention. Here are some steps that can be taken to prevent bullying:
            
            Raise awareness: Educate individuals about the harms of bullying and how it can be prevented.
            
            Foster a positive school or workplace culture: Encourage respect, kindness, and inclusivity in all settings.
            
            Implement effective policies and procedures: Develop clear guidelines for reporting and responding to incidents of bullying.
            
            Provide resources for victims and perpetrators: Offer counseling, support groups, and other resources to help those affected by bullying.
            
            Take action: Speak up if you witness bullying or suspect that someone is being bullied
        </p>
      </div>
@endsection