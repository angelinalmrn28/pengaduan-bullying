
@extends('app')

@section('content')
    <header class="home">
        <div class="container">
            <div class="row" id="judul-home">
                <div class="container text-center">
                    <div class="row" id="Home bully">

                      <div class="clearfix">
                        <img src="/img/foto.jpeg" class="col-md-3 float-md-end mb-3 ms-md-3" alt="...">
                      
                           <h2> Stop Bullying: Understanding, Preventing, and Taking Action</h2>
                           <p> Bullying is a pervasive issue that affects individuals of all ages and backgrounds. 
                            It is a problem that can lead to emotional distress, physical harm, and even death in some cases.
                             While bullying has been a problem for many years, the rise of technology 
                             and social media has made it easier 
                             for bullies to target their victims, making it more difficult for victims
                              to escape the abuse.
                             {{-- 2 --}}
                        </div>
                      </div><figure class="text-center">
                        <blockquote class="blockquote">
                          <p>Be yourself. Don't worry about what other people are thinking of you, because they're probably feeling the same kind of scared, horrible feelings that everyone does.”</p>
                        </blockquote>
                        <figcaption class="blockquote-footer">
                         <cite title="Source Title">Phil Lester</cite>
                        </figcaption>
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </header>
@endsection


