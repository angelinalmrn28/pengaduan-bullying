@extends('app')

@section('content')
<div class="container text-center">
<h3> Jenis Bullying</h3>
<br><br>


<div class="row row-cols-1 row-cols-md-3 g-4">
   <div class="col">
     <div class="card">
       <img src="/img/verbal bullying.jpeg" class="card-img-top" alt="..." style ="width:250"  >
       <div class="card-body">
         <h5 class="card-title">Verbal Bullying</h5>
         <p class="card-text">Perundungan verbal atau verbal bullying biasanya berupa kalimat kasar atau ejekan yang ditujukan pada seseorang..</p>
       </div>
     </div>
   </div>
   <div class="col">
     <div class="card">
       <img src="/img/physical bullying.jpeg" class="card-img-top" alt="..."style ="width:250" >
       <div class="card-body">
         <h5 class="card-title">Physical Bullying</h5>
         <p class="card-text"> korban sering menunjukkan ketakutan berlebih saat harus bertemu dengan pelakunya. Korban juga biasanya malas pergi ke sekolah, meminta pindah sekolah, atau menangis ketakutan saat teringat peristiwa bullying yang dialaminya..</p>
       </div>
     </div>
   </div>
   <div class="col">
     <div class="card">
       <img src="/img/social bullying.png" class="card-img-top" alt="..."  style ="width:250";>
       <div class="card-body">
         <h5 class="card-title">Social Bullying</h5>
         <p class="card-text">Contoh bullying sosial antara lain pengucilan atau intimidasi tidak langsung yang dilakukan secara berkelompok terhadap seseorang..</p>
       </div>
     </div>
   </div>
   <div class="col">
     <div class="card">
       <img src="/img/cyber bullying.jpg" class="card-img-top" alt="..." style ="width:150";>
       <div class="card-body">
         <h5 class="card-title">Cyber Bullying</h5>
         <p class="card-text">.</p> misalnya status atau unggahan gambar bernada negatif yang ditujukan pada seseorang dan obrolan via aplikasi chat yang mengintimidasi korban.
       </div>
     </div>
   </div>
   <div class="col">
      <div class="card">
        <img src="/img/sexualbully.jpeg" class="card-img-top" alt="..." style ="width:200>
        <div class="card-body">
          <h5 class="card-title">Sexual Bullying</h5>
          <p class="card-text">Sexual harassment atau pelecehan seksual juga dapat dikategorikan sebagai bullying karena pelakunya memiliki motif tendensi negatif..</p>
 </div>
</div>
<br>
</div> 
@endsection