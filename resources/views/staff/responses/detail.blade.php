@extends('app')

@section('content')
    <div class="container">
        <h1>Detail response</h1>
        <form action="/staff/responses/{{ $response->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-12 mb-12">
                    <label for="complaint_id" class="form-label">Complaint_id</label>
                    <input type="text" class="form-control" id="complaint_id" name="complaint_id"
                        value="{{ $response->complaint_id }}" disabled>
                </div>



                <div class="col-12 mb-12">
                    <label for="response_date" class="form-label">Response_date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date"
                        value="{{ $response->response_date }}">
                </div>



                <div class="col-12 mb-12">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response"
                        value="{{ $response->response }}">
                </div>



                <div class="col-12 mb-12">
                    <label for="staff_id" class="form-label">Staff_id</label>
                    <input type="text" class="form-control" id="staff_id" name="staff_id"
                        value="{{ $response->staff_id }}" disabled>
                </div>
                <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection