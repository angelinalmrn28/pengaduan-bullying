
@extends('app')

@section('content')
    <header class="home">
        <div class="container">
            <div class="row" id="judul-home">
                <div class="container text-center">
                    <div class="row" id="pengertian-bully">
                            <img src="/img/jenis-bullying.webp" style="width:30%">
                      <div class="row">
                        <div class="col-6" style="text-align: left" id="apa-itu">
                            <h1 id="text-apa">APA ITU BULLYING?</h1>
                            <p id="text-pengertian"> Bullying adalah penggunaan kekuatan, ancaman, atau intimidasi untuk menindas, melecehkan, atau mendominasi seseorang. Ini dapat terjadi dalam berbagai bentuk, termasuk kekerasan fisik, penyalahgunaan verbal, cyberbullying, dan pengecualian dari kelompok atau aktivitas. Ini dapat terjadi di sekolah, tempat kerja, online, atau di setiap tempat di mana orang berinteraksi.
                            </p> Mengapa bullying menjadi masalah?
                             Bullying dapat memiliki konsekuensi serius dan berkelanjutan bagi korban dan pelaku. Korban bullying sering mengalami kecemasan, depresi, dan rendah diri. Mereka juga mungkin mengalami masalah kesehatan fisik, seperti sakit kepala dan sakit perut, dan mungkin mengalami kesulitan dalam prestasi akademik atau sosial. Bullying juga dapat menyebabkan konsekuensi yang lebih serius, seperti bunuh diri dan self-harm.
                            Pelaku bullying juga berisiko mengalami dampak negatif. Mereka mungkin mengalami kesulitan dalam hubungan, kinerja akademik, dan kesehatan mental. Mereka juga berisiko menghadapi konsekuensi hukum, terutama dalam kasus cyberbullying.</p>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </header>
@endsection