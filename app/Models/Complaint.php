<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'location',
        'report_content',
        'image',
        'addressed_to',
        'type bullying',
        'status',
        'reporter_id',
        'victim_name',
        'incident_date',
        'created_at',
        'updated_at',

    ];
    public function student() 
    {
        return $this->belongsTo(Student::class, 'reporter_id');
    }
    public function responses()
    {
        return $this->hasMany(Responses::class);
    }

    

}
