<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'complaint_id',
        'response_date',
        'staff_id',
        'created_at',
        'updated_at',
    ];

    public function complaint()  
    {
        return $this->belongsTo(Complaint::class);
    }
    




}
