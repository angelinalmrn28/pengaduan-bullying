<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'class',
        'nis',
        'gender',
        'created_at',
        'updated_at',
    ];

    public function user() 
    {
        return $this->belongsTo(User::class);
    }

    public function complaints()
    {
        return $this->hasMany( Complaint::class, 'reporter_id');
    }
}
