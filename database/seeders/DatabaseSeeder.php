<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Complaint;
use App\Models\Response;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    $user_1=User::create([
        'name' => 'Agustinus',
        'username' => 'Agus2323',
        'password' => Hash::make('Tes_212'),
        'phone' => '0821345172701',
        'level' => 'student'
    ]);

    $user_2=User::create([
        'name' => 'Amelia',
        'username' => 'Amel3434',
        'password' => Hash::make('Tes_213'),
        'phone' => '0895432321432',
        'level' => 'student'
    ]);

    $user_3=User::create([
        'name' => 'Christian',
        'username' => 'Christ4545',
        'password' => Hash::make('Tes_214'),
        'phone' => '0878432567671',
        'level' => 'student'
    ]);

    $user_4=User::create([
        'name' => 'Catherine',
        'username' => 'Catherine5657',
        'password' => Hash::make('Tes_215'),
        'phone' => '0895431269797',
        'level' => 'student'
    ]);
    $user_5=User::create([
        'name' => 'Dinda',
        'username' => 'Dinda9898',
        'password' => Hash::make('Tes_216'),
        'phone' => '0817675432134',
        'level' => 'student'
    ]);


    $user_6=User::create([
        'name' => 'Admin',
        'username' => 'Admin',
        'password' => Hash::make('Admin_78965'),
        'phone' => '0821540605325',
        'level' => 'admin'
    ]);

    $user_7=User::create([
        'name' => 'Staff',
        'username' => 'Staff',
        'password' => Hash::make('Staff_0505'),
        'phone' => '0896810434678',
        'level' => 'staff'
    ]);


    $student_1=Student::create([
        'name' => 'Agustinus',
        'nis' => '0228769534',
        'class' => 'XII',
        'gender' => 'male',
        'user_id' => $user_1->id 
    ]);

    
    $student_2=Student::create([
        'name' => 'Amelia',
        'nis' => '0228134681',
        'class' => 'XI',
        'gender' => 'female',
        'user_id' => $user_2->id 
    ]);

    $student_3=Student::create([
        'name' => 'Christian',
        'nis' => '022790605',
        'class' => 'X',
        'gender' => 'male',
        'user_id' => $user_3->id 
    ]);

    
    $student_4=Student::create([
        'name' => 'Catherine',
        'nis' => '0225438753',
        'class' => 'IX',
        'gender' => 'female',
        'user_id' => $user_4->id 
    ]);

    
    $student_5=Student::create([
        'name' => 'Dinda',
        'nis' => '0221684074',
        'class' => 'VII',
        'gender' => 'female',
        'user_id' => $user_5->id 
    ]);


    $complaint_1=Complaint::create([
        'incident_date' => '2020-01-03',
        'location' => 'SMK Gracia Bandung (lapangan)',
        'report_content' =>' saya di ejek  di lapangan oleh Amel ',
        'image' => '',
        'addressed_to' => 'Amel',
        'type_bullying' => 'Verbal',
        'status' => 'process',
        'reporter_id' => $student_1->id, 
        'victim_name'=>'Amel',
    ]);


    $complaint_2=Complaint::create([
        'incident_date' => '2020-02-10',
        'location' => 'SMK Gracia Bandung ( Ruang 10)',
        'report_content' =>' saya di tendang di dekat Wc oleh Catherine ',
        'image' => '',
        'addressed_to' => 'Catherine',
        'type_bullying' => 'physical',
        'status' => 'unprocessed',
        'reporter_id' => $student_2->id ,
        'victim_name' => 'Catherine',

    ]);


    $complaint_3=Complaint::create([
        'incident_date' => '2020-01-15',
        'location' => 'SMP Gracia Bandung (Tangga)',
        'report_content' =>' saya di dorong oleh Christian(di tangga)  ',
        'image' => '',
        'addressed_to' => 'Christian',
        'type_bullying' => 'Physical',
        'status' => 'Process',
        'reporter_id' => $student_3->id,
        'victim_name' => 'Christian',
    ]);


    $complaint_4=Complaint::create([
        'incident_date' => '2020-01-25',
        'location' => 'Sekitar Sekolah Gracia Bandung ',
        'report_content' =>' saya di ejek sama  Hendri',
        'image' => '',
        'addressed_to' => 'Hendri',
        'type_bullying' => 'verbal',
        'status' => 'process',
        'reporter_id' => $student_4->id ,
        'victim_name' => 'Hendri',
    ]);


    $complaint_5=Complaint::create([
        'incident_date' => '2020-01-30',
        'location' => 'SMK Gracia Bandung (di Ruang 15)',
        'report_content' =>' saya di ejek  di ruang 15 oleh Dinda ',
        'image' => '',
        'addressed_to' => 'Dinda',
        'type_bullying' => 'Verbal',
        'status' => 'process',
        'reporter_id' => $student_5->id,
        'victim_name' => 'Dinda',
    ]);

    $responses_1=Response::create ([
        'responses_date' => '2020-01-05',
        'response' =>'Terima kasih, sudah melapor',
        'staff_id' => $user_1->id,
        'complaint_id' => $complaint_1->id,
    ]);

    $responses_2=Response::create([
        'responses_date' => '2020-02-13',
         'response' =>'Terima kasih, sudah melapor',
        'staff_id' => $user_2->id,
        'complaint_id' => $complaint_2->id,
    ]);

    $responses__3=Response::create([
        'responses_date' => '2020-01-17',
         'response' =>'Terima kasih, sudah melapor',
        'staff_id' => $user_3->id, 
        'complaint_id' => $complaint_3->id,
    ]);

    $responses_4=Response::create([
        'responses_date' => '2020-01-27',
         'response' =>'Terima kasih, sudah melapor',
        'staff_id' => $user_4->id,
        'complaint_id' => $complaint_4->id,
    ]);

    $responses_5=Response::create([
        'responses_date' => '2020-02-03',
         'response' =>'Terima kasih, sudah melapor',
        'staff_id' => $user_5->id,
        'complaint_id' => $complaint_5->id,
    ]);











































        // \App\Models\User::factory(10)->create();
    }
}
